import { useEffect, useState } from "react";

function Count() {
    const [count, setCount] = useState(0)
    const ClickHander = () =>{
        console.log("click");
        setCount(count + 1)
    }
    useEffect(() =>{
        document.title = `you clicked ${count} time`
    }, [count])
    return (
        <div>
            <p>you Clicked {count} Time</p>
            <button onClick={ClickHander}>Click</button>
        </div>
    )
}
export default Count;